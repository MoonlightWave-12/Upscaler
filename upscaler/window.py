# window.py: main window
#
# Copyright (C) 2022 Upscaler Contributors
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, version 3.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
#
# SPDX-License-Identifier: GPL-3.0-only

import filecmp
import logging
import os
import re
import subprocess
import sys
import tempfile
from locale import atof
from gettext import gettext as _
from typing import Any, Callable, Literal, Optional, cast

import vulkan  # type: ignore
from gi.repository import Adw, Gdk, Gio, GLib, GObject, Gtk, Pango
from PIL import Image, ImageChops, ImageOps  # type: ignore

from upscaler.app_profile import APP_ICON_NAME  # type: ignore
from upscaler.constants import ALG_WARNINGS
from upscaler.controller import UpscalerController
from upscaler.exceptions import AlgorithmFailed, AlgorithmWarning
from upscaler.utils import (
    MediaFile,
    ImageFile,
    FileChooser,
    image_formats,
    create_texture_from_img,
    open_file_in_external_program,
)
from upscaler.queue_row import UpscalerQueueRow
from upscaler.dimension_label import UpscalerDimensionLabel
from upscaler.scale_spin_button import UpscalerScaleSpinButton
from upscaler.threading import RunAsync


@Gtk.Template(resource_path="/io/gitlab/theevilskeleton/Upscaler/gtk/window.ui")
class UpscalerWindow(Adw.ApplicationWindow):
    __gtype_name__ = "UpscalerWindow"

    # Declare child widgets
    toast: Adw.ToastOverlay = Gtk.Template.Child()  # type: ignore
    stack_upscaler: Gtk.Stack = Gtk.Template.Child()  # type: ignore
    stack_picture: Gtk.Stack = Gtk.Template.Child()  # type: ignore
    status_welcome: Adw.StatusPage = Gtk.Template.Child()  # type: ignore
    button_input: Gtk.Button = Gtk.Template.Child()  # type: ignore
    button_open: Gtk.Button = Gtk.Template.Child()  # type: ignore
    input_dimension: UpscalerDimensionLabel = Gtk.Template.Child()  # type: ignore
    output_dimension: UpscalerDimensionLabel = Gtk.Template.Child()  # type: ignore
    button_upscale: Gtk.Button = Gtk.Template.Child()  # type: ignore
    loading_page_spinner: Adw.Spinner = Gtk.Template.Child()  # type: ignore
    loading_image_loading: Adw.Spinner = Gtk.Template.Child()  # type: ignore
    image: Gtk.Picture = Gtk.Template.Child()  # type: ignore
    # video = Gtk.Template.Child() # type: ignore
    combo_models: Adw.ComboRow = Gtk.Template.Child()  # type: ignore
    string_models: Gtk.StringList = Gtk.Template.Child()  # type: ignore
    spin_scale: UpscalerScaleSpinButton = Gtk.Template.Child()  # type: ignore
    button_output: Gtk.Button = Gtk.Template.Child()  # type: ignore
    label_output: Gtk.Label = Gtk.Template.Child()  # type: ignore
    drag_revealer: Gtk.Revealer = Gtk.Template.Child()  # type: ignore
    main_toolbar_view: Adw.ToolbarView = Gtk.Template.Child()  # type: ignore
    main_nav_view: Adw.NavigationView = Gtk.Template.Child()  # type: ignore
    options_page: Adw.PreferencesPage = Gtk.Template.Child()  # type: ignore
    queue_list_box: Gtk.ListBox = Gtk.Template.Child()  # type: ignore
    progress_list_box: Gtk.ListBox = Gtk.Template.Child()  # type: ignore
    completed_list_box: Gtk.ListBox = Gtk.Template.Child()  # type: ignore
    progress_group: Adw.PreferencesGroup = Gtk.Template.Child()  # type: ignore
    queue_group: Adw.PreferencesGroup = Gtk.Template.Child()  # type: ignore
    completed_group: Adw.PreferencesGroup = Gtk.Template.Child()  # type: ignore
    main_view_header_bar: Adw.HeaderBar = Gtk.Template.Child()  # type: ignore

    # Initialize function
    def __init__(self, **kwargs: Any) -> None:
        super().__init__(**kwargs)

        self.controller = UpscalerController()
        self.set_default_size(self.controller.get_width(), self.controller.get_height())
        if self.controller.is_maximized():
            self.maximize()

        # Check if hardware is supported
        if os.environ.get("DEBUG_DISABLE_VULKAN_CHECK") != "1":
            GLib.idle_add(self.__vulkaninfo)
        else:
            logging.warning("Skipping Vulkan check")

        # Remove `drag_revealer` because it messes up the inspector
        if os.environ.get("DEBUG_DISABLE_REVEALER") == "1":
            logging.warning("Disabling revealer")
            self.__disable_revealer()

        # Declare App (needed for notifications later)
        app = kwargs.get("application")
        if app is None:
            raise ValueError("Application should be passed to UpscalerWindow")
        self.app: Gio.Application = app

        # Set icon on welcome page
        self.status_welcome.set_icon_name(APP_ICON_NAME)

        # Declare default models and variables
        self.model_images = {
            "realesrgan-x4plus": _("Photo"),
            "realesrgan-x4plus-anime": _("Cartoon/Anime"),
        }

        # Update accessibility label for output button
        self.button_output.update_property(
            (Gtk.AccessibleProperty.LABEL,),
            (self.button_output.get_tooltip_text(),),
        )

        # Suppress "DecompressionBombWarning" warning
        # https://pillow.readthedocs.io/en/stable/reference/Image.html#PIL.Image.open
        Image.MAX_IMAGE_PIXELS = None

        self.process: Optional[subprocess.Popen[Any]] = None
        self.media_file: MediaFile = MediaFile()
        self.output_file_path: Optional[str] = None
        content = Gdk.ContentFormats.new_for_gtype(GObject.TYPE_NONE)  # type: ignore
        self.target = Gtk.DropTarget(formats=content, actions=Gdk.DragAction.COPY)
        self.target.set_gtypes((Gdk.Texture, Gio.File))
        self.string_models.splice(0, 0, list(self.model_images.values()))
        self.busy: bool = False

        # Connect signals
        self.connect("notify::is-active", self.__remove_notifications)
        self.button_input.connect("clicked", self.open_file)
        self.button_upscale.connect("clicked", self.__upscale)
        self.button_output.connect("clicked", self.__output_location)
        self.combo_models.connect("notify::selected", self.__set_model)
        self.target.connect("drop", self.__on_drop)
        self.target.connect("enter", self.__on_enter)
        self.target.connect("leave", self.__on_leave)
        self.stack_upscaler.connect("notify::visible-child", self.__on_child_changed)
        self.add_controller(self.target)

        self.stack_upscaler.set_visible_child_name("stack_welcome_page")
        self.current_stack_page_name: str = "stack_welcome_page"

        self.spin_scale.adjustment.connect(
            "value-changed", self.__update_post_upscale_image_size
        )

        # self.model_videos = [
        #     'realesr-animevideov3',
        # ]

    def __on_file_open(self, data: tuple[MediaFile, Gdk.Texture]) -> None:
        """Open and display file."""
        self.media_file, texture = data

        width, height = self.media_file.get_dimension()

        self.input_dimension.set_dimension_label(width, height)
        self.output_dimension.set_dimension_label(
            width * self.spin_scale.get_value(), height * self.spin_scale.get_value()
        )

        # Display image
        self.image.set_paintable(texture)

        self.stop_loading_animation()

        if (
            self.main_nav_view.find_page("upscaling-options")
            != self.main_nav_view.get_visible_page()
        ):
            self.main_nav_view.push_by_tag("upscaling-options")

    def open_file(self, *args: Any) -> None:
        """Open the file chooser to load the file."""
        FileChooser.open_file(self, self.on_load_file)

    def __output_location(self, *args: Any) -> None:
        """
        Select output file location.

        Widgets are updated to let the user continue
        """

        def good(output_file_path: str) -> None:
            # Set variables
            self.output_file_path = output_file_path

            # Update widgets
            self.button_upscale.set_sensitive(True)
            self.button_upscale.set_has_tooltip(False)

            # Trim long base name if necessary
            self.label_output.set_label(os.path.basename(self.output_file_path))
            self.label_output.set_ellipsize(Pango.EllipsizeMode.MIDDLE)

        def bad(message: Optional[str]) -> None:
            if message:
                self.toast.add_toast(Adw.Toast.new(message))

        if not self.media_file.original_path:
            return

        base_path = os.path.basename(os.path.splitext(self.media_file.original_path)[0])
        image_size = self.media_file.get_dimension()
        width = image_size[0]
        height = image_size[1]

        output_filename = f"{base_path}-{width}x{height}-upscaled.png"
        FileChooser.output_file(self, output_filename, good, bad)

    def __on_drop(self, _: Any, content: Gdk.Texture | Gio.File, *args: Any) -> None:
        """Load image when it has been dropped into the app."""

        # Check the type of dropped content
        if isinstance(content, Gio.File):
            self.on_load_file(content)
            return

        if isinstance(content, Gdk.Texture):
            tmp, _iostream = Gio.File.new_tmp()
            if not (path := tmp.get_path()):
                return

            content.save_to_png(path)
            self.on_load_file(tmp)
            return

        self.start_loading_animation()

    def __on_enter(self, *args: Any) -> Literal[Gdk.DragAction.COPY]:
        self.main_nav_view.add_css_class("blurred")
        self.drag_revealer.set_reveal_child(True)
        return Gdk.DragAction.COPY

    def __on_leave(self, *args: Any) -> None:
        self.main_nav_view.remove_css_class("blurred")
        self.drag_revealer.set_reveal_child(False)

    def __on_child_changed(self, *args: Any) -> None:
        child_name = self.stack_upscaler.get_visible_child_name()
        match child_name:
            case "stack_loading" | None:
                pass
            case _:
                self.current_stack_page_name = child_name
                is_welcome_page = child_name != "stack_welcome_page"
                self.main_view_header_bar.set_show_title(is_welcome_page)
                self.button_open.set_visible(is_welcome_page)

    def __upscale_progress(self, queue_row: UpscalerQueueRow, progress: str) -> None:
        """Updates upscale progress."""
        queue_row.progressbar.set_fraction(atof(progress) / 100)

    def __run_next(self) -> None:
        queue_row: Optional[UpscalerQueueRow] = cast(
            UpscalerQueueRow, self.progress_list_box.get_row_at_index(0)
        )
        queued_row: Optional[UpscalerQueueRow] = cast(
            UpscalerQueueRow, self.queue_list_box.get_row_at_index(0)
        )
        self.progress_group.set_visible(True)

        if hasattr(queue_row, "process"):
            self.queue_group.set_visible(bool(self.queue_list_box.get_row_at_index(0)))
            return
        elif not queue_row:
            if not queued_row:
                if not bool(self.completed_list_box.get_row_at_index(0)):
                    self.stack_upscaler.set_visible_child_name("stack_welcome_page")
                self.busy = False
                self.progress_group.set_visible(False)
                return

            queue_row = queued_row
            self.queue_list_box.remove(queued_row)
            self.queue_group.set_visible(bool(self.queue_list_box.get_row_at_index(0)))

        self.progress_list_box.append(queue_row)

        # Run in a separate thread
        def run(queue_row: UpscalerQueueRow) -> None:
            input_path = self.media_file.get_preferred_input_path()
            if None in (
                input_path,
                self.output_file_path,
            ):
                error_message = _("Unexpected error while running the algorithm")
                raise AlgorithmFailed(0, error_message)

            selected_model = list(self.model_images)[self.combo_models.get_selected()]
            queue_row.command = [
                "upscayl-bin",
                # fmt: off
                "-i",
                str(input_path),
                "-o",
                str(self.output_file_path),
                "-n",
                str(selected_model),
                "-s",
                str(self.spin_scale.get_value()),
                # fmt: on
            ]

            queue_row.progressbar.set_text(_("Loading"))
            queue_row.progressbar.set_visible(True)
            queue_row.button_cancel.set_valign(Gtk.Align.START)
            queue_row.run()

            cmd = " ".join(queue_row.command)
            logging.info(f"Running: {cmd}")

            # Read each line, query the percentage and update the progress bar
            output = ""
            bad = False
            if queue_row.process.stderr is not None:
                for line in iter(queue_row.process.stderr.readline, ""):
                    logging.info(line)
                    output += line
                    if (res := re.match(r"^(\d*.\d+)%$", line)) is not None:
                        GLib.idle_add(self.__upscale_progress, queue_row, res.group(1))
                        continue
                    else:
                        # Check if this line is a warning
                        if bad:
                            continue
                        for warn in ALG_WARNINGS:
                            bad = bad or re.match(warn, line) is not None

            # Process algorithm output
            result = queue_row.process.poll()
            if queue_row.cancelled:
                logging.info("Manually cancelled upscaling by the user")

            elif result != 0:
                error_value = 0 if result is None else result
                raise AlgorithmFailed(error_value, output)

            if bad:
                raise AlgorithmWarning

        # Run after run() function finishes
        def callback(result: Gio.AsyncResult, error: Optional[Exception]) -> None:
            self.progress_list_box.remove(queue_row)

            if not queue_row.cancelled:
                self.upscaling_completed_dialog(queue_row, error)

            self.__remove_file(queue_row.temporary_path)

            self.__run_next()

        # Run functions asynchronously
        RunAsync(run, callback, queue_row)
        self.busy = True

    def __upscale(self, *args: Any) -> None:
        """Initialize algorithm and updates widgets."""

        queue_row = UpscalerQueueRow()
        queue_row.original_path = self.media_file.original_path
        queue_row.temporary_path = self.media_file.temporary_path

        queue_row.set_title(os.path.basename(str(self.output_file_path)))
        queue_row.connect("aborted", self.__cancel)
        queue_row.progressbar.set_visible(False)
        queue_row.button_cancel.set_valign(Gtk.Align.CENTER)
        self.progress_list_box.set_visible(True)
        self.queue_list_box.append(queue_row)
        self.__run_next()

        self.stack_upscaler.set_visible_child_name("stack_queue_page")
        self.main_nav_view.pop()
        self.button_upscale.set_sensitive(False)
        self.label_output.set_label(_("(None)"))
        self.button_upscale.set_has_tooltip(True)

    def upscaling_completed_dialog(
        self, queue_row: UpscalerQueueRow, error: Optional[Exception]
    ) -> None:
        """Ask the user if they want to open the file."""
        if self.output_file_path is None:
            return

        toast = None

        notification = Gio.Notification()
        notification.set_body(
            _("Upscaled {path}").format(path=os.path.basename(self.output_file_path))
        )

        operation = _("Open")
        action_name = "app.open-output"
        output_file_variant = GLib.Variant("s", self.output_file_path)

        self.completed_list_box.append(queue_row)
        self.completed_group.set_visible(True)
        queue_row.button_cancel.set_visible(False)
        queue_row.progressbar.set_show_text(False)
        queue_row.progressbar.set_margin_top(12)

        def set_up_open_in_external_program() -> None:
            queue_row.button_open.set_visible(True)
            queue_row.button_open.connect(
                "clicked",
                lambda _button, file_path: open_file_in_external_program(file_path),
                self.output_file_path,
            )

        # Display success
        if error is None:
            queue_row.add_css_class("success")
            set_up_open_in_external_program()

            notification.set_title(_("Upscaling Completed"))

            notification.set_default_action_and_target(action_name, output_file_variant)
            notification.add_button_with_target(
                operation, action_name, output_file_variant
            )

        # Display success with warnings
        elif isinstance(error, AlgorithmWarning):
            queue_row.add_css_class("warning")
            set_up_open_in_external_program()

            toast = Adw.Toast(
                title=_("Image upscaled with warnings"),
                timeout=0,
            )
            self.toast.add_toast(toast)

            notification.set_title(_("Upscaling Completed With Warnings"))

            notification.set_default_action_and_target(action_name, output_file_variant)
            notification.add_button_with_target(
                operation, action_name, output_file_variant
            )

        # Display error dialog with error message
        else:
            queue_row.add_css_class("error")
            dialog = Adw.AlertDialog.new(_("Error While Upscaling"))
            sw = Gtk.ScrolledWindow()
            sw.set_min_content_height(200)
            sw.set_min_content_width(400)
            sw.add_css_class("card")

            text = Gtk.Label()
            text.set_label(str(error))
            text.set_margin_top(12)
            text.set_margin_bottom(12)
            text.set_margin_start(12)
            text.set_margin_end(12)
            text.set_xalign(0)
            text.set_yalign(0)
            text.add_css_class("monospace")
            text.set_wrap(True)
            text.set_wrap_mode(Pango.WrapMode.WORD_CHAR)

            sw.set_child(text)
            dialog.set_extra_child(sw)

            def error_response(dialog: Adw.AlertDialog, response_id: str) -> None:
                dialog.close()

                if response_id != "copy":
                    return

                if (display := Gdk.Display.get_default()) is not None:
                    clipboard = display.get_clipboard()
                    clipboard.set(str(error))
                    toast = Adw.Toast.new(_("Error copied to clipboard"))
                    self.toast.add_toast(toast)

            dialog.add_response("ok", _("_Dismiss"))
            dialog.connect("response", error_response)
            dialog.add_response("copy", _("_Copy to Clipboard"))
            dialog.set_response_appearance("copy", Adw.ResponseAppearance.SUGGESTED)
            dialog.present(self)

            notification.set_title(_("Upscaling Failed"))
            notification.set_body(_("Error while processing"))

        if not self.props.is_active:
            self.app.send_notification("upscaling-done", notification)

    def __set_model(self, *args: Any) -> None:
        """Set model and print."""
        selected_model_name = list(self.model_images)[self.combo_models.get_selected()]
        message = f"Model name: {selected_model_name}"
        logging.info(message)

    # Update post-upscale image size as the user adjusts the spinner
    def __update_post_upscale_image_size(self, *args: Any) -> None:
        factor = self.spin_scale.adjustment.get_value()
        width, height = self.media_file.get_dimension()
        self.output_dimension.set_dimension_label(
            width * factor,
            height * factor,
        )

    def close_dialog(self, function: Callable[[], None]) -> None:
        """Prompt the user to stop the algorithm when it is running."""
        dialog = Adw.AlertDialog.new(
            _("Stop Upscaling?"),
            _("All progress will be lost"),
        )

        def response(dialog: Adw.AlertDialog, response_id: str) -> None:
            if response_id == "stop":
                function()

        dialog.add_response("cancel", _("_Cancel"))
        dialog.add_response("stop", _("_Stop"))
        dialog.set_response_appearance("stop", Adw.ResponseAppearance.DESTRUCTIVE)
        dialog.connect("response", response)
        dialog.present(self)

    def start_loading_animation(self) -> None:
        """Show loading screen."""
        self.stack_upscaler.set_visible_child_name("stack_loading")
        self.stack_picture.set_visible_child_name("stack_loading")
        self.options_page.set_sensitive(False)

    def stop_loading_animation(self) -> None:
        """Close loading screen."""
        self.stack_upscaler.set_visible_child_name(self.current_stack_page_name)
        self.stack_picture.set_visible_child_name("stack_picture")
        self.options_page.set_sensitive(True)

    def __cancel(self, queue_row: UpscalerQueueRow, *args: Any) -> None:
        """Stop algorithm."""
        if hasattr(queue_row, "process"):
            queue_row.cancelled = True
            queue_row.process.kill()
        else:
            self.queue_list_box.remove(queue_row)

        self.__remove_file(queue_row.temporary_path)

        self.__run_next()

    def __load_file(self, file: Gio.File) -> tuple[MediaFile, Gdk.Texture]:
        """Attempt to load image."""
        file_info = file.query_info(
            Gio.FILE_ATTRIBUTE_STANDARD_CONTENT_TYPE, Gio.FileQueryInfoFlags.NONE, None
        )
        content_type = file_info.get_content_type()

        # Check if file is supported
        if content_type not in image_formats:
            raise Exception(f"unsupported image {file.get_path()}")

        image_file = ImageFile()
        image_file.original_path = file.get_path()
        image = Image.open(image_file.original_path)

        if transposed := self.__transpose_and_load_img(image):
            image, image_file.temporary_path = transposed

        image.thumbnail((512, 512))
        texture = create_texture_from_img(image)
        image.close()

        return (image_file, texture)

    def __transpose_and_load_img(self, img: Image) -> Optional[tuple[Image, str]]:
        img_transposed = ImageOps.exif_transpose(img)
        diff = ImageChops.difference(img, img_transposed)

        if not diff.getbbox():
            return None

        # Creates temporary file to store transposed image
        temporary_path = tempfile.mkstemp(suffix=f".{img.format}")[1]
        img_transposed.save(temporary_path, quality=100, subsampling=0)

        logging.info(f"Transposed image and temporarily saved to “{temporary_path}”")

        return (img_transposed, temporary_path)

    def on_load_file(self, file: Gio.File) -> None:
        """Load a given file."""

        def callback(
            data: tuple[MediaFile, Gdk.Texture],
            error: Optional[Exception],
        ) -> None:
            if error is None:
                self.__on_file_open(data)
                return

            message = _("“{path}” is not a valid image").format(
                path=os.path.basename(file.get_path() or "?")
            )
            self.toast.add_toast(Adw.Toast.new(message))

            self.stop_loading_animation()

        if self.__compare(self.media_file.original_path, file.get_path()):
            if (
                self.main_nav_view.find_page("upscaling-options")
                != self.main_nav_view.get_visible_page()
            ):
                self.main_nav_view.push_by_tag("upscaling-options")
            return

        self.__remove_file(self.media_file.temporary_path)

        self.start_loading_animation()
        logging.info(f"Input file: {file.get_path()}")

        RunAsync(self.__load_file, callback, file)

    def __compare(self, file1: Optional[str], file2: Optional[str]) -> bool:
        """Compare two files."""
        if None in (file1, file2):
            return False
        return filecmp.cmp(str(file1), str(file2))

    def __remove_file(self, path: Optional[str]) -> None:
        try:
            os.remove(cast(str, path))
            logging.info(f"Removed temporary file at “{path}”")
        except (TypeError, OSError):
            pass

    def __vulkaninfo(self) -> None:
        """Check if Vulkan works."""
        try:
            vulkan.vkCreateInstance(vulkan.VkInstanceCreateInfo(), None)
        except (vulkan.VkErrorIncompatibleDriver, OSError):
            logging.critical("Error: Vulkan drivers not found")
            title = _("Incompatible or Missing Vulkan Drivers")
            subtitle = _(
                "The Vulkan drivers are either not installed or incompatible with the hardware. Please make sure that the correct Vulkan drivers are installed for the appropriate hardware."
            )

            dialog = Adw.AlertDialog.new(title, subtitle)
            dialog.add_response("exit", _("_Exit Upscaler"))
            dialog.connect("response", lambda *args: sys.exit(1))
            dialog.present(self)

    def __disable_revealer(self) -> None:
        self.drag_revealer.set_visible(False)

    def __remove_notifications(self, *args: Any) -> None:
        self.app.withdraw_notification("upscaling-done")

    def do_close_request(self) -> bool:
        """Prompt user to stop the algorithm if it's running."""
        if not self.busy:
            return False

        def function() -> None:
            """Save window size"""
            self.controller.set_width(self.get_width())
            self.controller.set_height(self.get_height())
            self.controller.set_maximized(self.is_maximized())

            sys.exit()

        self.close_dialog(function)
        return True

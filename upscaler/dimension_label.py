# dimension_label.py: Label for setting dimensions.
#
# Copyright (C) 2024 Upscaler Contributors
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, version 3.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
#
# SPDX-License-Identifier: GPL-3.0-only


from gi.repository import Gtk


@Gtk.Template(
    resource_path="/io/gitlab/theevilskeleton/Upscaler/gtk/dimension-label.ui"
)
class UpscalerDimensionLabel(Gtk.Box):
    __gtype_name__ = "UpscalerDimensionLabel"

    width: Gtk.Label = Gtk.Template.Child()  # type: ignore
    height: Gtk.Label = Gtk.Template.Child()  # type: ignore

    def get_dimension_label(self) -> tuple[int, int]:
        """Get the dimensions of the label."""
        return (int(self.width.get_label()), int(self.height.get_label()))

    def set_dimension_label(self, width: int, height: int) -> None:
        """Set the dimensions of the label."""
        self.width.set_label(str(int(width)))
        self.height.set_label(str(int(height)))
